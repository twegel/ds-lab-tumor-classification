# Tumor Droplet Classification

This is the repository for tumor droplet classification, created as part of the Data Science Lab at ETH Zürich in Fall 2022. Contributors: Johan Lokna, Fredrik Nestaas, Viggo Moro, Florian Hübler, Tobias Wegel.

## Setup

Necessary software: The code has been tested with Python 3.8 and above.

1. Create a virtual environment with python 3.8 or above.
2. Clone the repository to your environment using ```git clone https://gitlab.ethz.ch/twegel/ds-lab-tumor-classification```.
3. install the required libraries using ```pip install -r requirements.txt```.

You should be good to go!

## Usage

Usage is meant to be done mainly through ```main.ipynb```. 

### Further details

For more info, you can read the corresponding report here:
[https://www.overleaf.com/read/qzwdmjzcczmb](https://www.overleaf.com/read/qzwdmjzcczmb)
Note that currently this is a preliminary version of the report.

You can also look at the slides from the project demo by opening `DSLab_demo.pdf`.

