import sys
import os
import json
from jsonschema import validate

# OMP error
os.environ['KMP_DUPLICATE_LIB_OK']='True'

from ..pipeline.dataset.make_dataset import make_dataset, view_samples
from ..pipeline.training.train import train
from ..pipeline.models.predict import predict, make_dataset_for_model
from ..pipeline.models.check_predictions import check_predictions, view_sample_predictions

with open("pipeline/utils/schema_train.json") as f:
    schema_train = json.load(f)
with open("pipeline/utils/schema_predict.json") as f:
    schema_predict = json.load(f)
with open("pipeline/utils/schema_preprocess.json") as f:
    schema_preprocess = json.load(f)

args_preprocess = {
    "input_dir":"data/ColonCancer/",
    "image_input_dir":"data/ColonCancer/ImageFiles/",
    "dataset_name":"Dataset_full",
    "extract_labels":True,
    "num_samples":10,
    "overwrite":True,
    "padding":(250,250),
    "FGlinspace": {"theta":100,"r":1000},
    "transforms":["Identity"],
    "validators":["NonEmpty"],
    "detectors":["IsolationForest","LocalFactor"],
    "detector_comp_style":"union"
}
try: 
    validate(instance=args_preprocess, schema=schema_preprocess)
except: 
    raise RuntimeError("the arguments file is not valid")

make_dataset(args_preprocess)


args_train = {
    "data_dir": "data/ColonCancer/Dataset_full/",
    "model_dir": "models/",
    "wb": False,
    "pre_trained": "timm-resnet34",
    "model_name": "resnet34-fulldata",
    "random_seed": 42,
    "batch_size": 50, 
    "learning_rate": 1e-4,
    "class_weights":[1/0.3,1/0.5,1/0.15,1/0.15],
    "epochs": 100, 
    "test_size": 0.05,
    "padding_dim": 250, 
    "validate_every": 150,
    "freeze": False,
}
try: 
    validate(instance=args_train, schema=schema_train)
except: 
    raise RuntimeError("the arguments file is not valid")

train(args_train)

# args_predict = {
#     "input_dir": "data/ColonCancer/",
#     "image_input_dir" : "data/ColonCancer/TestImage/",
#     "dataset_name":"DatasetPredict",
#     "extract_labels":True,
#     "pred_name": "predictions",
#     "model_dir": "models/my-new-model_2022-12-15-14-44/",
#     "model_name": "my-new-model"
# }
# try: 
#     validate(instance=args_predict, schema=schema_predict)
# except: 
#     raise RuntimeError("the arguments file is not valid")

# make_dataset_for_model(args_predict)

# predict(args_predict)