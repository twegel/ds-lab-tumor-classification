#!/bin/bash

#SBATCH --job-name=make_dataset
#SBATCH -n 24
#SBATCH --time=10:00:00
#SBATCH --mem-per-cpu=2048
#SBATCH --output=output_dataset.txt
#SBATCH --error=error_dataset.txt
#SBATCH --mail-type=ALL

cd .. ;
python make_dataset.py --dataset_dir data/ColonCancer/TrainDataset --image_dir data/ColonCancer/TrainImages --scoring_dir data/ColonCancer/ScoringFiles --temp_dir data/ColonCancer/PreprocessingFiles ;
python make_dataset.py --dataset_dir data/ColonCancer/TestDataset --image_dir data/ColonCancer/TestImages --scoring_dir data/ColonCancer/ScoringFiles --temp_dir data/ColonCancer/PreprocessingFiles --hist_dir data/ColonCancer/TrainDataset ;