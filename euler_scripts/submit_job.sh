#!/bin/bash

#SBATCH --job-name=train
#SBATCH -n 8
#SBATCH --time=10:00:00
#SBATCH --mem-per-cpu=2048
#SBATCH --gpus=1
#SBATCH --output=output_train.txt
#SBATCH --error=error_train.txt
#SBATCH --mail-type=ALL

cd .. ; python main.py --dataset_dir data/ColonCancer/TrainDataset --model-name binary_model --predict-dead