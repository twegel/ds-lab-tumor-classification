#!/bin/bash

#SBATCH --job-name=test_backbone
#SBATCH -n 8
#SBATCH --time=12:00:00
#SBATCH --mem-per-cpu=2048
#SBATCH --gpus=4
#SBATCH --output=output.txt
#SBATCH --error=error.txt

python example_TL_ResNet50.py


