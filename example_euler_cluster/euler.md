How to run jobs on Euler
========================
This document is to serve as a guide for how models can be trained on the cluster

Login
-----
To access the cluster, you need to have ssh installed and then simply write ssh eth-usernamev@euler.ethz.ch in the terminal. You will then be asked to login with your eth password.

Setup
----
The following steps install the right dependencies on the cluster. I made use of Miniconda which seems to be the easiest option and this only needs to be done once.

 ### Download and install [Miniconda](https://docs.conda.io/en/latest/miniconda.html)
1. Run `wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh`.
2. Run `chmod +x Miniconda3-latest-Linux-x86_64.sh`.
3. Run `./Miniconda3-latest-Linux-x86_64.sh`.
4. Review and accept the license agreement.
5. Make sure the installation location is `/cluster/home/USERNAME/miniconda3` where `USERNAME` is your ETH username and press *ENTER* to confirm the location.
6. When asked whether you wish the installer to initialize Miniconda3, answer `yes`.
7. Disconnect and reconnect to the cluster.
8. Run `conda config --set auto_activate_base false`.
9. Run `rm Miniconda3-latest-Linux-x86_64.sh` to remove the installer.

After miniconda has been installed, you can make use of conda environmets as usual

### Create the conda environment
1. Run `conda deactivate` to make sure that you are starting from a clean state.
2. Run `conda create -n env-name` and confirm the prompts with *y*.
3. Run `conda activate env-name`.
4. Install the dependencies using pip or conda, e.g `pip install -r requirements.txt`
5. Whenever you change *requirements.txt*, do the following:
    1. Always run `conda activate env-name` and make sure the environment is activated *before* running any `pip` commands!
    2. If you added a new package, you need to re-run `pip install -r requirements.txt`.
    3. If you removed a package, you need to run `pip uninstall PACKAGE` where `PACKAGE` is the package name.
6. If the environment is no longer needed, you can free some space by removing the environment via `conda env remove -n env-name`

Uploading/downloading to the cluster
------------------------------------
To upload/download files and directories to/from the cluster, the easiest option I found was to use scp. Specifically, it can be done in the following way:

### Upload directory to Euler
`scp -r dummy_dir username@euler.ethz.ch:`

### Upload file to Euler
`scp dummy_file username@euler.ethz.ch:`

### Download file from Euler
`scp username@euler.ethz.ch:dummy_file .`
This downloads the file to the directory you are currently in.

Running code/jobs on the cluster
--------------------------------
The first thing you should do when you want to run something on the cluster is to 
To run code on the cluster, you need to submit a job. The easiest way this is done is by submitting a job script. Specifically, the following steps need to be taken:

1. Activate the conda environment by running `conda activate env-name`
2. Submit a job by running `sbatch < submit_job.sh`. This script controls what python code you want to run and also the resources you request for it (e.g number of GPPUs)




