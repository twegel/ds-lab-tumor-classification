# This scrips trains a ResNet50 model with transfer learning on the droplets. Moreover, the feature extractor is fixed and only the last fully 
# connected layer is retrained. This is intended to serve as an example of how models can be trained on the cluster as well as a baseline to compare 
# future methods against. No preprocessing of the data is performed except for what's required by the ResNet50 to match input format etc.

# Important, this assumes that individual droplets reside in the parent directory in the data folder as a big numpy array. However, this can be 
# changed at lines 35-36. Moreover, the first time you run the script the pretrained model will be automatically donwloaded. However, this doesn't 
# wor well on the cluster and the easiest option is to add the resnet50-11ad3fa6.pth file manually to home_dir/.cache/torch/hub/checkpoints

# This is needed to avoid "OMP: Error #15: Initializing libomp.dylib, but found libiomp5.dylib already initialized." error??
import os
import time
os.environ['KMP_DUPLICATE_LIB_OK']='True'

import numpy as np
import torch
from torch.utils.data import DataLoader
from torch.optim import lr_scheduler
from torchvision.models import resnet50, ResNet50_Weights
from sklearn.metrics import f1_score

from dataset import DropletDataset

# # Path to save model
# DIR_NAME = 'saved_models'


# # Create directory to save model if it doesn't exist
# os.chdir('..')
# parent_dir = os.path.abspath(os.curdir)
# path_save = os.path.join(parent_dir, DIR_NAME)
# if not os.path.exists(path_save):
        

# Load data
path_droplets = os.path.join(os.path.dirname(__file__), '../data/droplet_images.npy')
path_labels = os.path.join(os.path.dirname(__file__), '../data/labels.npy')

droplet_images = np.load(path_droplets)
labels = np.load(path_labels)

# Convert to 3-channel depth so that pre-trained models can be used
droplet_images = np.repeat(droplet_images, 3, axis=1)

# Split into train and test
idx = int(len(labels) * 0.8)
droplet_images_train, droplet_images_test = droplet_images[:idx,:,:,:], droplet_images[idx:,:,:,:]
labels_train, labels_test = labels[:idx], labels[idx:]

# Model
weights = ResNet50_Weights.DEFAULT
transform = weights.transforms()
model = resnet50(weights=weights)

# Dataloaders
train_dataset = DropletDataset(droplet_images_train, labels_train, transform)
test_dataset = DropletDataset(droplet_images_test, labels_test, transform)

train_dataloader = DataLoader(train_dataset, batch_size=32, shuffle=True) 
test_dataloader = DataLoader(test_dataset)


def train_model(dataloders, model, criterion, optimizer, scheduler, num_epochs=25):
    best_model_wts = model.state_dict()
    best_acc = 0.0
    best_f1_score = 0
    best_epoch = None
    dataset_sizes = {'train': len(dataloders['train'].dataset), 
                     'valid': len(dataloders['valid'].dataset)}

    for epoch in range(num_epochs):
        for phase in ['train', 'valid']:
            if phase == 'train':
                model.train(True)
            else:
                model.train(False)

            running_loss = 0.0
            running_corrects = 0
            total_preds = np.array([])
            total_labels = np.array([])

            for inputs, labels in dataloders[phase]:
                if use_gpu:
                    inputs, labels = inputs.cuda(), labels.cuda()
                else:
                    inputs, labels = inputs, labels

                optimizer.zero_grad()   # We want to set graident to zero at the beginning of each mini batch

                outputs = model(inputs)
                _, preds = torch.max(outputs.data, 1)   # Used since we want most likley class from predicted probabilities, returns probabilities, classes
                loss = criterion(outputs, labels)

                if phase == 'train':
                    loss.backward()
                    optimizer.step()
                    scheduler.step()

                running_loss += loss.data.item()
                running_corrects += torch.sum(preds == labels.data)     # Used to compute accuracy

                # Append so that f1 score can f1-score can be computed for 
                total_preds = np.append(total_preds, preds.cpu().numpy())
                total_labels = np.append(total_labels, labels.cpu().numpy())
            
            if phase == 'train':
                train_epoch_loss = running_loss / dataset_sizes[phase]
                train_epoch_acc = running_corrects / dataset_sizes[phase]
                train_epoch_f1_score = f1_score(total_labels, total_preds, average='micro')
            else:
                valid_epoch_loss = running_loss / dataset_sizes[phase]
                valid_epoch_acc = running_corrects / dataset_sizes[phase]
                valid_epoch_f1_score = f1_score(total_labels, total_preds, average='micro')
                
            if phase == 'valid' and valid_epoch_f1_score > best_f1_score:
                best_f1_score = valid_epoch_f1_score
                best_acc = valid_epoch_acc
                best_epoch = epoch + 1
                best_model_wts = model.state_dict()

        print('Epoch [{}/{}] train loss: {:.4f} acc: {:.4f} f1 score: {:.4f} ' 
              'valid loss: {:.4f} acc: {:.4f} f1 score: {:.4f}'.format(
                epoch + 1, num_epochs,
                train_epoch_loss, train_epoch_acc, train_epoch_f1_score, 
                valid_epoch_loss, valid_epoch_acc, valid_epoch_f1_score))
            
    print('Best val acc: {:4f} f1 score: {:4f} for epoch {:d}'.format(best_acc, best_f1_score, best_epoch))

    model.load_state_dict(best_model_wts)
    return model


# ------------------------------------------------
# Here, ResNet is used as a fixed feature extractor 
# ------------------------------------------------

# use gpu if available
use_gpu = torch.cuda.is_available()

# freeze all model parameters
for param in model.parameters():
    param.requires_grad = False

# New final layer with 4 classes (replaces fully connected layers in backbone)
# Parameters of newly constructed layers have requires_grad=True by default
num_ftrs = model.fc.in_features     # This is the number of input features
model.fc = torch.nn.Linear(num_ftrs, 4)

if use_gpu:
    model = model.cuda()

criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.fc.parameters(), lr=0.001)
exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)   # Decays the learning rate of each parameter group by gamma every step_size epochs

data_loaders = {'train':train_dataloader, 'valid':test_dataloader}

start_time = time.time()
model = train_model(data_loaders, model, criterion, optimizer, exp_lr_scheduler, num_epochs=30)

# Save model
# path = os.path.join(path_save, "ResNet50.pth")
# torch.save(model.state_dict(), path)   # Save model
torch.save(model.state_dict(), 'ResNet50.pth')   # Save model

print('Training time: {:10f} minutes'.format((time.time()-start_time)/60))
print('GPU = ' + use_gpu)