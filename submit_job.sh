#!/bin/bash

#SBATCH --job-name=test_pipeline
#SBATCH -n 8
#SBATCH --time=12:00:00
#SBATCH --mem-per-cpu=2048
#SBATCH --gpus=4
#SBATCH --output=output.txt
#SBATCH --error=error.txt
#SBATCH --mail-type=ALL

python main.py